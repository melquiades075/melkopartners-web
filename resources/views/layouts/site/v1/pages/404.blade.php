@extends('layouts.site.v1.base')

@section('slider_style')
    @include('layouts.site.v1.partials.head.style')
    @include('layouts.site.v1.partials.head.smart-slider')
@endsection()

@section('slider_config')
    @include('layouts.site.v1.partials.head.slider-config')
@endsection()

@section('content')
    <section class="error-page ptb100 parallax" data-paroller-factor="0.1" data-paroller-type="background" data-paroller-direction="vertical">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-xs-12 text-right">
                    <div class="inline-block text-center">
                        <h1 class="error-title color-1 m0 fsize-210 fweight-700 lheight-150">
                            404
                        </h1>
                        <div class="color-2 fsize-40 fweight-500 lheight-normal mt30">
                        {{System::getSettings()['404_title']}}

                        </div>
                        <form action="#" class="mt30">
                            <div class="mb20">{{System::getSettings()['404_description']}}</div>
                            <div class="input-wrap inline-block relative">
                                
                            </div>
                        </form>
                        <div class="mt20">&nbsp;</div>
                        <a href="{{env('APP_URL')}}" class="btn theme-bg-gradient lheight-60 color-white fsize-14 fweight-600 mt20 plr30">{{System::getSettings()['404_button']}}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection()
