@extends('layouts.site.v1.base')

@section('slider_style')
    @include('layouts.site.v1.partials.head.style')
    @include('layouts.site.v1.partials.head.smart-slider')
@endsection()

@section('slider_config')
    @include('layouts.site.v1.partials.head.slider-config')
@endsection()

@section('content')
    <section class="wow fadeIn" data-wow-duration="1s">
        <div class="container-fluid">
            <div class="row">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d832.5280613013941!2d-70.59406651184777!3d-33.42031787943649!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662cf147518acff%3A0x6235b96d1707529b!2sSan+Crescente+434%2C+Las+Condes%2C+Regi%C3%B3n+Metropolitana!5e0!3m2!1sen!2scl!4v1527891286468" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </section>
    
    <section class="ptb120">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="m0">{{System::getSettings()['contact_title']}}</h4>
                    <p class="mt30">
                    <p class="mt30">
                        {{System::getSettings()['contact_description']}}
                    </p>
                    <div class="contact-bl mtb5">
                        <div class="contact-item table">
                            <div class="table-row">
                                <div class="icon-bl table-cell valign-top pr15">
                                    <i class="fa fa-phone text-gradient fsize-42" aria-hidden="true"></i>
                                </div>
                                <div class="table-cell valign-top">
                                    <div class="fsize-12 uppercase">
                                        Telefonos
                                    </div>
                                    <div class="second-row fsize-20 fweight-500 color-2">
                                        @foreach(System::getSettings()['phones'] as $phone)
                                            <br/> Tel. {{$phone}}
                                        @endforeach()
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="contact-item table">
                            <div class="table-row">
                                <div class="icon-bl table-cell valign-top pr15">
                                    <i class="fa fa-map-marker text-gradient fsize-42" aria-hidden="true"></i>
                                </div>
                                <div class="table-cell valign-top">
                                    <div class="fsize-12 uppercase">
                                        Dirección
                                    </div>
                                    <div class="second-row fsize-20 fweight-500 color-2">
                                        {{System::getSettings()['address']}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="contact-item table">
                            <div class="table-row">
                                <div class="icon-bl table-cell valign-top pr15">
                                    <i class="fa fa-pencil text-gradient fsize-42" aria-hidden="true"></i>
                                </div>
                                <div class="table-cell valign-top">
                                    <div class="fsize-12 uppercase">
                                        Correo
                                    </div>
                                    <div class="second-row email-bl">
                                        @foreach(System::getSettings()['emails'] as $email)
                                            <br/><a href="mailto:{{$email}}" class="line_link  td-underline">{{$email}}</a>
                                        @endforeach()
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4 class="m0">Formulario de contacto</h4>
                    <form action="#" class="contact-us-form mt30">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <input type="text" class="general_input italic" placeholder="Nombre">
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <input type="email" class="general_input italic" placeholder="Correo">
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <input type="email" class="general_input italic" placeholder="Telefono">
                            </div>
                            <div class="col-md-12">
                                <textarea class="general_input italic" placeholder="Tu mensaje..."></textarea>
                            </div>
                        </div>
                        <!--
                        <input type="submit" class="btn theme-bg-gradient plr30 fweight-600 fsize-14 color-white mt10" value="SEND MESSAGE">
                        -->
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection()
