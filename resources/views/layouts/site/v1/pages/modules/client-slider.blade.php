<section class="clients" style="background-color: #f2f2f2">
    <div class="container-fluid">
        <div class="row">
            <div class="client-slider">
                @foreach(System::getPartners() as $client)
                    <div class="col-client">
                        <a href="{{$client['url']}}" target="_blank">
                            <img src="{{env('APP_URL')}}/assets/v1/images/clients/{{$client['logo']}}" alt="{{$client['name']}}">
                        </a>
                    </div>
                @endforeach()
            </div>
        </div>
    </div>
</section>
