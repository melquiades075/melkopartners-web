<section class="page-name ptb100 parallax" data-paroller-factor="0.1" data-paroller-type="background" data-paroller-direction="vertical" style="  background-image: url('/assets/v1/images/{{System::getSettings()['image_header']}}');">
    <div class="container">
        <div class="row">
            <h1 class="page-title text-center mb15 mt0 color-white">{!!$title!!}</h1>
            <div class="breadcrumbs text-center color-white uppercase fsize-14 fweight-600">
                <a href="{{env('APP_URL')}}" class="color-white inline-block valign-middle"><i class="fa fa-home"></i></a> /
                <span class="fsize-14 fweight-600 inline-block valign-middle">{{$title}}</span>
            </div>
        </div>
    </div>
</section>
