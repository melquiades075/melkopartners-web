<section class="ptb120">
    <div class="container">
        @foreach(System::getPartners() as $client)
            <div class="client-item row">
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 wow fadeIn" data-wow-duration="2s">
                    <div class="client-image">
                        <img src="{{env('APP_URL')}}/assets/v1/images/clients/{{$client['images']}}" alt="" class="img-responsive">
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="partition_left pl30 relative mtb40">
                        <div class="ptb5">
                            <div class="subtitle">
                                {{$client['before_title']}}
                            </div>
                            <div class="title color-2">
                                <span>{{$client['name']}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="lheight-30">
                        <p class="mb0">
                            {{$client['description']}}
                        </p>
                    </div>
                    <a href="{{$client['url']}}" target="_blank" class="btn light border-gradient color-16 fsize-14 fweight-600 mt40">
                        <span class="block plr30">{{System::getSettings()['client_label_button']}}</span>
                    </a>
                </div>
            </div>
        @endforeach()
    </div>
</section>
