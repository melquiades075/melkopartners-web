<!-- SLIDER SECTION START -->
<section class="main-slider">
    <div id="n2-ss-2-align" class="n2-ss-align">
        <div class="n2-padding">
            <div id="n2-ss-2" data-creator="Smart Slider 3" class="n2-ss-slider n2-ow n2-has-hover n2notransition n2-ss-load-fade " data-minFontSizedesktopPortrait="1"
                data-minFontSizedesktopLandscape="1" data-minFontSizetabletPortrait="1" data-minFontSizetabletLandscape="1"
                data-minFontSizemobilePortrait="1" data-minFontSizemobileLandscape="1" style="font-size: 16px;" data-fontsize="16">
                <div class="n2-ss-slider-1 n2-ss-swipe-element n2-ow" >
                    <div class="n2-ss-slider-2 n2-ow">
                        <div class="n2-ss-slider-3 n2-ow" >
                            <div class="n2-ss-slide-backgrounds"></div>
                                @foreach(System::getSlides() as $slide)
                                    <div  data-slide-duration="0" data-id="{{$loop->index}}" class="n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-4" >
                                        <div data-hash="0e67b5acf6f66aa6aab659eaa99128be" data-desktop="{{env('APP_URL')}}/assets/v1/images/plugins/slider/images/{{$slide['image']}}" class="n2-ss-slide-background n2-ow"
                                            data-opacity="1" data-blur="0" data-mode="fill" data-x="50" data-y="50">
                                            <div class="n2-ss-slide-background-mask" >
                                                <img title="" src="{{env('APP_URL')}}/assets/v1/images/plugins/slider/images/{{$slide['image']}} " alt="" />
                                            </div>
                                        </div>
                                        <div class="n2-ss-layers-container n2-ow" data-csstextalign="center" >
                                            <div class="n2-ss-section-outer" >
                                                <div class="n2-ss-layer n2-ow" style="overflow:visible;" data-csstextalign="inherit" data-desktopportraitmaxwidth="0" data-cssselfalign="inherit"
                                                    data-desktopportraitselfalign="inherit" data-pm="content" data-desktopportraitpadding="80|*|10|*|10|*|10|*|px+"
                                                    data-tabletportraitpadding="30|*|10|*|10|*|10|*|px+" data-mobileportraitpadding="30|*|10|*|10|*|10|*|px+"
                                                    data-desktopportraitinneralign="inherit" data-type="content" data-rotation="0"
                                                    data-desktopportrait="1" data-desktoplandscape="1" data-tabletportrait="1" data-tabletlandscape="1"
                                                    data-mobileportrait="1" data-mobilelandscape="1" data-adaptivefont="1" data-desktopportraitfontsize="100"
                                                    data-plugin="rendered">
                                                    <div class="n2-ss-section-main-content n2-ss-layer-content n2-ow" style="padding:3.125em 0.625em 0.625em 0.625em ;" data-verticalalign="center">
                                                        <div class="n2-ss-layer n2-ow" style="margin:0em 0em 0em 0em ;overflow:visible;" data-pm="normal" data-desktopportraitmargin="0|*|0|*|0|*|0|*|px+"
                                                            data-desktopportraitheight="0" data-desktopportraitmaxwidth="0" data-cssselfalign="center"
                                                            data-desktopportraitselfalign="center" data-type="layer" data-rotation="0"
                                                            data-animations="eyJzcGVjaWFsWmVybyI6MCwidHJhbnNmb3JtT3JpZ2luIjoiNTB8Knw1MHwqfDAiLCJpbiI6W3sibmFtZSI6IlRvcCBmYWRlIiwiZHVyYXRpb24iOjEuMiwiZGVsYXkiOjAuNSwib3BhY2l0eSI6MCwieSI6NDAwfV19"
                                                            data-desktopportrait="1" data-desktoplandscape="1" data-tabletportrait="1"
                                                            data-tabletlandscape="1" data-mobileportrait="1" data-mobilelandscape="1"
                                                            data-adaptivefont="0" data-desktopportraitfontsize="100" data-tabletportraitfontsize="60"
                                                            data-mobileportraitfontsize="40" data-plugin="rendered">
                                                            <div id="n2-ss-2item1" class="n2-font-12c29df1ae3183b9e513e47e0dc93e48-hover   n2-ow fweight-500i" style="display:block;">
                                                                {!!$slide['title']!!} 
                                                                <br /> 
                                                            </div>
                                                        </div>
                                                        <div class="n2-ss-layer n2-ow slide-desc" style="margin:1.875em 0em 3.125em 0em ;overflow:visible;" data-pm="normal" data-desktopportraitmargin="30|*|50|*|30|*|50|*|px+"
                                                            data-mobileportraitmargin="20|*|50|*|20|*|50|*|px+" data-desktopportraitheight="0"
                                                            data-desktopportraitmaxwidth="0" data-cssselfalign="center" data-desktopportraitselfalign="center"
                                                            data-type="layer" data-rotation="0" data-animations="eyJzcGVjaWFsWmVybyI6MCwidHJhbnNmb3JtT3JpZ2luIjoiNTB8Knw1MHwqfDAiLCJpbiI6W3sibmFtZSI6IkJvdHRvbSBmYWRlIiwib3BhY2l0eSI6MCwieSI6LTQwMH1dfQ=="
                                                            data-desktopportrait="1" data-desktoplandscape="1" data-tabletportrait="1"
                                                            data-tabletlandscape="1" data-mobileportrait="1" data-mobilelandscape="1"
                                                            data-adaptivefont="0" data-desktopportraitfontsize="100" data-mobileportraitfontsize="80"
                                                            data-plugin="rendered">
                                                            <div class="n2-ow n2-ow-all n2-ss-desktop n2-ss-mobile n2-ss-tablet">
                                                                <p class="n2-font-5cc30673b6d569891ae72deca15a0a44-paragraph   n2-ow">
                                                                    {!!$slide['text']!!}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="n2-ss-layer n2-ow hidden" style="margin:0em 0em 0em 0em ;overflow:visible;" data-pm="normal" data-desktopportraitmargin="20|*|0|*|0|*|0|*|px+"
                                                            data-desktopportraitheight="0" data-desktopportraitmaxwidth="0" data-cssselfalign="inherit"
                                                            data-desktopportraitselfalign="inherit" data-csstextalign="inherit" data-desktopportraitpadding="10|*|10|*|10|*|10|*|px+"
                                                            data-mobileportraitpadding="0|*|0|*|0|*|0|*|px+" data-desktopportraitgutter="20"
                                                            data-mobileportraitgutter="10" data-desktopportraitwrapafter="0" data-mobileportraitwrapafter="1"
                                                            data-mobilelandscapewrapafter="1" data-desktopportraitinneralign="inherit"
                                                            data-type="row" data-rotation="0" data-animations="eyJzcGVjaWFsWmVybyI6MCwidHJhbnNmb3JtT3JpZ2luIjoiNTB8Knw1MHwqfDAiLCJpbiI6W3sibmFtZSI6IkJvdHRvbSBmYWRlIiwiZHVyYXRpb24iOjEuMiwiZGVsYXkiOjAuNSwib3BhY2l0eSI6MCwieSI6LTQwMH1dfQ=="
                                                            data-desktopportrait="1" data-desktoplandscape="1" data-tabletportrait="1"
                                                            data-tabletlandscape="1" data-mobileportrait="1" data-mobilelandscape="1"
                                                            data-adaptivefont="0" data-desktopportraitfontsize="100" data-plugin="rendered">
                                                            <div class="n2-ss-layer-row " style="padding:0.625em 0.625em 0.625em 0.625em ;">
                                                                <div class="n2-ss-layer n2-ow" style="width: 50%;margin-right: 20px;margin-left: 20px;margin-top: 20px;overflow:visible;"
                                                                    data-csstextalign="inherit" data-desktopportraitmaxwidth="0"
                                                                    data-pm="default" data-desktopportraitpadding="10|*|0|*|10|*|10|*|px+"
                                                                    data-mobileportraitpadding="0|*|0|*|0|*|0|*|px+" data-desktopportraitinneralign="inherit"
                                                                    data-desktopportraitorder="0" data-type="col" data-rotation="0"
                                                                    data-desktopportrait="1" data-desktoplandscape="1" data-tabletportrait="1"
                                                                    data-tabletlandscape="1" data-mobileportrait="1" data-mobilelandscape="1"
                                                                    data-adaptivefont="0" data-desktopportraitfontsize="100" data-plugin="rendered">
                                                                    <div class="n2-ss-layer-col n2-ss-layer-content" style="padding:0.625em 0em 0.625em 0.625em ;" data-verticalalign="center">
                                                                        <div class="n2-ss-layer n2-ow btn theme-bg-gradient plr50 fsize-14 fweight-600" style="margin:0em 0em 0em 0em ;overflow:visible;"
                                                                            data-pm="normal" data-desktopportraitmargin="0|*|0|*|0|*|0|*|px+"
                                                                            data-desktopportraitheight="0" data-desktopportraitmaxwidth="0"
                                                                            data-cssselfalign="right" data-desktopportraitselfalign="right"
                                                                            data-mobileportraitselfalign="center" data-type="layer"
                                                                            data-rotation="0" data-desktopportrait="1" data-desktoplandscape="1"
                                                                            data-tabletportrait="1" data-tabletlandscape="1" data-mobileportrait="1"
                                                                            data-mobilelandscape="1" data-adaptivefont="0" data-desktopportraitfontsize="100"
                                                                            data-plugin="rendered">
                                                                            <div class="n2-ss-button-container n2-ow n2-font-3b57680072a9c80793aafc49fb57a3e1-link  n2-ss-nowrap">
                                                                                <a class="n2-style-b6f61f3fc1f6fdbb3b03f32919900f2d-heading  n2-ow " onclick="return false;" href="#">
                                                                                    <span>
                                                                                        <span class="slider_btn fsize-14 fweight-600 uppercase font-1">Appointment</span>
                                                                                    </span>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="n2-ss-layer n2-ow" style="width: 50%;margin-right: 20px;margin-left: 20px;margin-top: 20px;overflow:visible;"
                                                                    data-csstextalign="inherit" data-desktopportraitmaxwidth="0"
                                                                    data-pm="default" data-desktopportraitpadding="10|*|10|*|10|*|0|*|px+"
                                                                    data-mobileportraitpadding="0|*|0|*|0|*|0|*|px+" data-desktopportraitinneralign="inherit"
                                                                    data-desktopportraitorder="0" data-type="col" data-rotation="0"
                                                                    data-desktopportrait="1" data-desktoplandscape="1" data-tabletportrait="1"
                                                                    data-tabletlandscape="1" data-mobileportrait="1" data-mobilelandscape="1"
                                                                    data-adaptivefont="0" data-desktopportraitfontsize="100" data-plugin="rendered">
                                                                    <div class="n2-ss-layer-col n2-ss-layer-content" style="padding:0.625em 0.625em 0.625em 0em ;" data-verticalalign="center">
                                                                        <div class="n2-ss-layer n2-ow btn white plr50 fsize-14 fweight-600" style="margin:0em 0em 0em 0em ;overflow:visible;" data-pm="normal"
                                                                            data-desktopportraitmargin="0|*|0|*|0|*|0|*|px+" data-desktopportraitheight="0"
                                                                            data-desktopportraitmaxwidth="0" data-cssselfalign="left"
                                                                            data-desktopportraitselfalign="left" data-mobileportraitselfalign="center"
                                                                            data-type="layer" data-rotation="0" data-desktopportrait="1"
                                                                            data-desktoplandscape="1" data-tabletportrait="1" data-tabletlandscape="1"
                                                                            data-mobileportrait="1" data-mobilelandscape="1" data-adaptivefont="0"
                                                                            data-desktopportraitfontsize="100" data-plugin="rendered">
                                                                            <div class="n2-ss-button-container n2-ow n2-font-f56bf4defabbb98ed47fcfe4c99b0d3f-link  n2-ss-nowrap">
                                                                                <a class="n2-style-b6f61f3fc1f6fdbb3b03f32919900f2d-heading  n2-ow " onclick="return false;" href="portfolio-extended.html">
                                                                                    <span>
                                                                                        <span class="slider_btn fsize-14 fweight-600 uppercase font-1">Our Portfolio</span>
                                                                                    </span>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach()
                            </div>
                        </div>
                    </div>
                    <div data-ssleft="0+180" data-sstop="height/2-previousheight/2" id="n2-ss-2-arrow-previous" class="n2-ss-widget n2-ss-widget-display-desktop n2-ss-widget-display-tablet n2-ss-widget-display-mobile nextend-arrow n2-ow nextend-arrow-previous  nextend-arrow-animated-fade n2-ib"
                        style="position: absolute;" role="button" aria-label="Previous slide" tabindex="0">
                        <img class="n2-ow" data-no-lazy="1" data-hack="data-lazy-src" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzIiIGhlaWdodD0iMzIiIHZpZXdCb3g9IjAgMCAzMiAzMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJNMTEuNDMzIDE1Ljk5MkwyMi42OSA1LjcxMmMuMzkzLS4zOS4zOTMtMS4wMyAwLTEuNDItLjM5My0uMzktMS4wMy0uMzktMS40MjMgMGwtMTEuOTggMTAuOTRjLS4yMS4yMS0uMy40OS0uMjg1Ljc2LS4wMTUuMjguMDc1LjU2LjI4NC43N2wxMS45OCAxMC45NGMuMzkzLjM5IDEuMDMuMzkgMS40MjQgMCAuMzkzLS40LjM5My0xLjAzIDAtMS40MmwtMTEuMjU3LTEwLjI5IiBmaWxsPSIjZmZmZmZmIiBvcGFjaXR5PSIwLjgiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvc3ZnPg=="
                            alt="Arrow" />
                    </div>
                    <div data-ssright="0+180" data-sstop="height/2-nextheight/2" id="n2-ss-2-arrow-next" class="n2-ss-widget n2-ss-widget-display-desktop n2-ss-widget-display-tablet n2-ss-widget-display-mobile nextend-arrow n2-ow nextend-arrow-next  nextend-arrow-animated-fade n2-ib"
                        style="position: absolute;" role="button" aria-label="Next slide" tabindex="0">
                        <img class="n2-ow" data-no-lazy="1" data-hack="data-lazy-src" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzIiIGhlaWdodD0iMzIiIHZpZXdCb3g9IjAgMCAzMiAzMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJNMTAuNzIyIDQuMjkzYy0uMzk0LS4zOS0xLjAzMi0uMzktMS40MjcgMC0uMzkzLjM5LS4zOTMgMS4wMyAwIDEuNDJsMTEuMjgzIDEwLjI4LTExLjI4MyAxMC4yOWMtLjM5My4zOS0uMzkzIDEuMDIgMCAxLjQyLjM5NS4zOSAxLjAzMy4zOSAxLjQyNyAwbDEyLjAwNy0xMC45NGMuMjEtLjIxLjMtLjQ5LjI4NC0uNzcuMDE0LS4yNy0uMDc2LS41NS0uMjg2LS43NkwxMC43MiA0LjI5M3oiIGZpbGw9IiNmZmZmZmYiIG9wYWNpdHk9IjAuOCIgZmlsbC1ydWxlPSJldmVub2RkIi8+PC9zdmc+"
                            alt="Arrow" />
                    </div>
                    <div data-ssleft="width/2-bulletwidth/2" data-ssbottom="0+60" data-offset="60" class="n2-ss-widget n2-ss-widget-display-desktop n2-ss-widget-display-tablet n2-ss-widget-display-mobile n2-ss-control-bullet"
                        style="position: absolute;">
                        <div class=" nextend-bullet-bar n2-ow nextend-bullet-bar-horizontal" style="text-align: center;">
                            @foreach(System::getSlides() as $slide)
                                <div class="n2-ow n2-style-ca7f94bbdd373614357d3bb2f06a4bb9-dot " tabindex="0"></div>
                            @endforeach()
                        </div>
                    </div>
                </div>
            </div>
            <div class="n2-clear"></div>
            <div id="n2-ss-2-spinner" style="display: none;">
                <div>
                    <div class="n2-ss-spinner-simple-white-container">
                        <div class="n2-ss-spinner-simple-white"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="n2-ss-2-placeholder" style="position: relative;z-index:2;color:RGBA(0,0,0,0);">
        <img style="width: 100%; max-width:3000px; display: block;" class="n2-ow" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMCIgd2lkdGg9IjE5MjAiIGhlaWdodD0iNTAwIiA+PC9zdmc+"
            alt="Slider" />
    </div>
</section>
<!-- SLIDER SECTION END -->
