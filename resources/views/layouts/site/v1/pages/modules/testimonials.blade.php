<!-- TESTIMONIALS SECTION START -->
<section class="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="testimonials-slider background-11">
                @foreach(System::getWeWork() as $work)
                    <div>
                        <div class="testimonial-img col-lg-6 col-md-6 col-sm-12 col-xs-12" style="background-image: url(/assets/v1/images/{{$work['image']}});"></div>
                        <div class="testimonial-content-bl col-lg-6 col-md-6 col-sm-12 col-xs-12 ptb100">
                            <div class="testimonial-wrapper">
                                <div class="partition_left inline-block pl30 relative mb40">
                                    <div class="ptb5">
                                        <div class="subtitle">
                                            {{$work['before_title']}}
                                        </div>
                                        <div class="title">
                                            <span  style="font-size:25px">{{$work['title']}}<span class="color-15">.</span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="testimonial-bg inline-block ptb30 italic fsize-20">
                                    <p class="mb10">
                                        «{{$work['description']}}»
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach()
            </div>
        </div>
    </div>
</section>
<!-- TESTIMONIALS SECTION END -->

