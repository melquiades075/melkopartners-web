@extends('layouts.site.v1.base')

@section('slider_style')
    @include('layouts.site.v1.partials.head.style')
    @include('layouts.site.v1.partials.head.smart-slider')
@endsection()

@section('slider_config')
    @include('layouts.site.v1.partials.head.slider-config')
@endsection()

@section('content')
    @include('layouts.site.v1.pages.modules.slider')
    <section class="services pt100">
        <div class="container">
            <div class="row">
                <div class="text-center wow fadeIn" data-wow-duration="2s">
                    <!--
                    <div class="subtitle">
                        Get all the advantages
                    </div>
                    -->
                    <div class="title color-2 lheight-40 mb30">
                        <span>MELKO<span class="color-15">&</span>PARTNERS</span>
                    </div>
                    <div class="spliter mb20"></div>
                    <p><span class="color-15"><b>Melko&Partners</b></span> es un “micro holding” compuesto por un conjunto de empresas internacionales que busca maximizar <br>la rentabilidad del grupo utilizando las sinergias que se dan entre ellas. Siempre respetando la autonomía de cada una.</p>
                    <p><span class="color-15"><b>Melko&Partners</b></span> tiene operaciones en <b>Chile</b>, <b>Ecuador</b>, <b>Colombia</b>, <b>Ecuador</b> e <b>Israel</b>.</p>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.site.v1.pages.modules.testimonials')
@endsection()
