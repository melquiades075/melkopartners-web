@extends('layouts.site.v1.base')

@section('slider_style')
    @include('layouts.site.v1.partials.head.style')
    @include('layouts.site.v1.partials.head.smart-slider')
@endsection()

@section('slider_config')
    @include('layouts.site.v1.partials.head.slider-config')
@endsection()

@section('content')
    @include('layouts.site.v1.pages.modules.header')
    <section class="faq-accordeon ptb100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul class="accordion panel-group" id="collapse-group1">
                        <li class="accordion-item panel panel-default">
                            <a href="#spoiler-1" data-toggle="collapse" data-parent="#collapse-group1" class="block td-none ptb20 plr30" aria-expanded="true">
                                <i class="fa fa-pencil pr20" aria-hidden="true"></i>
                                <span class="accordion-title uppercase fsize-14 fweight-600">Rentabilidad</span>
                            </a>
                            <div class="collapse in spoiler-hidden plr20" id="spoiler-1">
                                <div class="open-bl pt40 pb20">
                                    <p>
                                    El generar una estructura central de control financiero y legal que permite vincular las diferentes UEN para generar oportunidades que de por sí solas serían más costosas.
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="accordion-item panel panel-default">
                            <a href="#spoiler-2" data-toggle="collapse" data-parent="#collapse-group1" class="block td-none ptb20 plr30 collapsed">
                                <i class="fa fa-pencil pr20" aria-hidden="true"></i>
                                <span class="accordion-title uppercase fsize-14 fweight-600">Sustentabilidad</span>
                            </a>
                            <div class="collapse spoiler-hidden plr20" id="spoiler-2">
                                <div class="open-bl pt40 pb20">
                                    <p>El generar alianzas estratégicas, partnerships o ventas cruzadas permite disminuir el riesgo y por su tamaño tienen la posibilidad de adaptarse con mayor flexibilidad al mercado.</p>
                                </div>
                            </div>
                        </li>
                        <li class="accordion-item panel panel-default">
                            <a href="#spoiler-3" data-toggle="collapse" data-parent="#collapse-group1" class="block td-none ptb20 plr30 collapsed">
                                <i class="fa fa-pencil pr20" aria-hidden="true"></i>
                                <span class="accordion-title uppercase fsize-14 fweight-600">Beneficios para la sociedad</span>
                            </a>
                            <div class="collapse spoiler-hidden plr20" id="spoiler-3">
                                <div class="open-bl pt40 pb20">
                                    <p>Nuestros productos normalmente son más eco friendly y nuestros servicios buscan mejorar la calidad de vida de las personas.</p>
                                </div>
                            </div>
                        </li>
                        <li class="accordion-item panel panel-default">
                            <a href="#spoiler-4" data-toggle="collapse" data-parent="#collapse-group1" class="block td-none ptb20 plr30 collapsed">
                                <i class="fa fa-pencil pr20" aria-hidden="true"></i>
                                <span class="accordion-title uppercase fsize-14 fweight-600">Ofrecemos</span>
                            </a>
                            <div class="collapse spoiler-hidden plr20" id="spoiler-4">
                                <div class="open-bl pt40 pb20">
                                    <ul>
                                        <li>Control y estandarización contable y financiera</li>
                                        <li>Control y estandarización jurídica</li>
                                        <li>Sinergias comerciales</li>
                                        <li>Modelos de apertura de negocios o UEN en otros países</li>
                                    </ul>    
                                </div>
                            </div>
                        </li>
                        <li class="accordion-item panel panel-default">
                            <a href="#spoiler-5" data-toggle="collapse" data-parent="#collapse-group1" class="block td-none ptb20 plr30 collapsed">
                                <i class="fa fa-pencil pr20" aria-hidden="true"></i>
                                <span class="accordion-title uppercase fsize-14 fweight-600">Nuestro equipo</span>
                            </a>
                            <div class="collapse spoiler-hidden plr20" id="spoiler-5">
                                <div class="open-bl pt40 pb20">
                                    <p>Nuestro modelo consiste en involucrar a los equipos de trabajo en una búsqueda continua de I+d+i para la mejora de nuestros productos y servicios. Los equipos de trabajo son interdisciplinarios e interculturales los cuales aportan perspectivas diferentes que en coordinación ofrecen una solución más creativa y rentable.
Queremos contribuir a generar un cambio de mentalidad a la hora de enfrentar los desafíos del presente.</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

@endsection()
