@extends('layouts.site.v1.base')

@section('head')

@endsection()

@section('content')
    @include('layouts.site.v1.pages.modules.header')
    <section class="about-text pt100 background-white relative">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-16">
                    <div class="partition_left pl30 relative mb60">
                        <div class="ptb5">
                            <div class="title color-2">
                                <span>Quienes <span class="color-15">Somos</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="lheight-30">
                        <p>
                        Melko&Partners es un “micro holding” compuesto por un conjunto de empresas internacionales que busca maximizar la rentabilidad del grupo utilizando las sinergias que se dan entre ellas. Siempre respetando la autonomía de cada una.

                        </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-16">
                    <div class="partition_left pl30 relative mb60">
                        <div class="ptb5">
                            <div class="title color-2">
                                <span>¿Qué <span class="color-15">ofrecemos</span>?</span>
                            </div>
                        </div>
                    </div>
                    <div class="lheight-30">
                        <p>
                        Este nuevo <a href="{{route('model')}}"><b>modelo</b></a> ofrece a sus stakeholders la posibilidad de aportar a la generación de valor, a través de un sistema sustentable que busca no solo la rentabilidad económica, sino también ofrecer nuevas vías para innovar y contribuir al cambio de nuestra sociedad.
                        </p>
                    </div>
                </div>            
            </div>
        </div>
    </section>
    <section class="about-text pt100 background-white relative">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <div class="partition_left pl30 relative mb60">
                        <div class="ptb5">
                            <div class="title color-2">
                                <span>Decidamos <span class="color-15">juntos</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="lheight-30">
                        <p>
                            Ofrecemos asesoría legal y contable centralizada como herramientas para conseguir tener mayores insights de la información financiera que se maneja en relación a cada empresa. De esa manera, se logra tomar decisiones estratégicas más acertadas, de forma individual, así como, coordinadas con el conjunto del “microholding”.
                        </p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-8 col-sm-offset-2 col-md-offset-0 about-sizer-img wow fadeInRight" data-wow-duration="2s">
                    <img src="{{env('APP_URL')}}/assets/v1/images/about/about-image.png" alt="">
                </div>
            </div>
        </div>
    </section>
@endsection()

@section('footer')

@endsection()