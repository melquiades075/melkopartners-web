<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>{{System::getSettings()['title']}}</title>
<!-- Favicon -->
<link rel="shortcut icon" href="{{env('APP_URL')}}/assets/v1/images/{{System::getSettings()['favicon']}}" type="image/x-icon">
<!-- Google Fonts -->
<link href="{{env('APP_URL')}}/assets/v1/fonts.googleapis.com/css65a5.css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">
<link href="{{env('APP_URL')}}/assets/v1/fonts.googleapis.com/css7fc9.css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
<!-- Font Awesome CSS -->
<link rel="stylesheet" href="{{env('APP_URL')}}/assets/v1/fonts/css/font-awesome.min.css?ver={{env('CCS_VERSION')}}" />
<!-- Bootstrap-->
<link rel="stylesheet" href="{{env('APP_URL')}}/assets/v1/css/bootstrap.min.css?ver={{env('CCS_VERSION')}}" />
<!-- Style -->
<link rel="stylesheet" href="{{env('APP_URL')}}/assets/v1/css/style.css?ver={{env('CCS_VERSION')}}" />
<!-- Responsive Style -->
<link rel="stylesheet" href="{{env('APP_URL')}}/assets/v1/css/responsive.css?ver={{env('CCS_VERSION')}}" />
<!-- Sliders -->
<link rel="stylesheet" type="text/css" href="{{env('APP_URL')}}/assets/v1/plugins/slider/css/n2-ss-2.css?ver={{env('CCS_VERSION')}}" media="screen, print" />
<link rel="stylesheet" type="text/css" href="{{env('APP_URL')}}/assets/v1/plugins/slick-1.8.0/slick/slick.css?ver={{env('CCS_VERSION')}}" />
<link rel="stylesheet" type="text/css" href="{{env('APP_URL')}}/assets/v1/plugins/slick-1.8.0/slick/slick-theme.css?ver={{env('CCS_VERSION')}}" />
<!-- Light Box -->
<link href="{{env('APP_URL')}}/assets/v1/plugins/lightbox2-master/dist/css/lightbox.css?ver={{env('CCS_VERSION')}}" rel="stylesheet">
<!-- Video js -->
<link href="{{env('APP_URL')}}/assets/v1/css/video-js.css?ver={{env('CCS_VERSION')}}" rel="stylesheet">
<!-- Animate CSS -->
<link rel="stylesheet" href="{{env('APP_URL')}}/assets/v1/plugins/animate.css-master/animate.min.css?ver={{env('CCS_VERSION')}}">
<!-- Datapicker -->
<link href="{{env('APP_URL')}}/assets/v1/plugins/jquery-date-range-picker-master/dist/daterangepicker.min.css?ver={{env('CCS_VERSION')}}" rel="stylesheet">
@yield('slider_style')
<script type="text/javascript" src="{{env('APP_URL')}}/assets/v1/plugins/slider/js/n2.js?ver={{env('JS_VERSION')}}"></script>
<script type="text/javascript" src="{{env('APP_URL')}}/assets/v1/plugins/slider/js/nextend-gsap.min.js?ver={{env('JS_VERSION')}}"></script>
<script type="text/javascript" src="{{env('APP_URL')}}/assets/v1/plugins/slider/js/nextend-frontend.min.js?ver={{env('JS_VERSION')}}"></script>
<script type="text/javascript" src="{{env('APP_URL')}}/assets/v1/plugins/slider/js/smartslider-frontend.min.js?ver={{env('JS_VERSION')}}"></script>
<script type="text/javascript" src="{{env('APP_URL')}}/assets/v1/plugins/slider/js/smartslider-simple-type-frontend.min.js?ver={{env('JS_VERSION')}}"></script>
<script type="text/javascript" src="{{env('APP_URL')}}/assets/v1/plugins/slider/js/nextend-webfontloader.min.js?ver={{env('JS_VERSION')}}"></script>
<script type="text/javascript" src="{{env('APP_URL')}}/assets/v1/plugins/slider/js/n2-ss-2.js?ver={{env('JS_VERSION')}}"></script>
@yield('slider_config')