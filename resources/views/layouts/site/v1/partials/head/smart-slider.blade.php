<script type="text/javascript">
    window.N2PRO = 1;
    window.N2GSAP = 1;
    window.N2PLATFORM = "native";
    window.nextend = {
        localization: {},
        deferreds: [],
        loadScript: function (url) {
            n2jQuery.ready(function () {
                var d = n2.Deferred();
                nextend.deferreds.push(d);
                n2.ajax({
                    url: url,
                    dataType: "script",
                    cache: true,
                    complete: function () {
                        setTimeout(function () {
                            d.resolve()
                        })
                    }
                })
            })
        },
        ready: function (cb) {
            n2.when.apply(n2, nextend.deferreds).done(function () {
                cb.call(window, n2)
            })
        }
    };
    window.N2SSPRO = 1;
    window.N2SS3C = "smartslider3";
    nextend.fontsLoaded = false;
    nextend.fontsLoadedActive = function () {
        nextend.fontsLoaded = true;
    };
    var fontData = {
        google: {
            families: ["Roboto:300,400:latin"]
        },
        active: function () {
            nextend.fontsLoadedActive()
        },
        inactive: function () {
            nextend.fontsLoadedActive()
        }
    };
    if (typeof WebFontConfig !== 'undefined') {
        var _WebFontConfig = WebFontConfig;
        for (var k in WebFontConfig) {
            if (k == 'active') {
                fontData.active = function () {
                    nextend.fontsLoadedActive();
                    _WebFontConfig.active();
                }
            } else if (k == 'inactive') {
                fontData.inactive = function () {
                    nextend.fontsLoadedActive();
                    _WebFontConfig.inactive();
                }
            } else if (k == 'google') {
                if (typeof WebFontConfig.google.families !== 'undefined') {
                    for (var i = 0; i < WebFontConfig.google.families.length; i++) {
                        fontData.google.families.push(WebFontConfig.google.families[i]);
                    }
                }
            } else {
                fontData[k] = WebFontConfig[k];
            }
        }
    }
    if (typeof WebFont === 'undefined') {
        window.WebFontConfig = fontData;
    } else {
        WebFont.load(fontData);
    }
</script>
