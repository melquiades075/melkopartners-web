<script type="text/javascript">
    window.n2jQuery.ready((function ($) {
        window.nextend.ready(function () {

            nextend.fontsDeferred = n2.Deferred();
            if (nextend.fontsLoaded) {
                nextend.fontsDeferred.resolve();
            } else {
                nextend.fontsLoadedActive = function () {
                    nextend.fontsLoaded = true;
                    nextend.fontsDeferred.resolve();
                };
                var intercalCounter = 0;
                nextend.fontInterval = setInterval(function () {
                    if (intercalCounter > 3 || document.documentElement.className.indexOf(
                            'wf-active') !== -1) {
                        nextend.fontsLoadedActive();
                        clearInterval(nextend.fontInterval);
                    }
                    intercalCounter++;
                }, 1000);
            }
            new N2Classes.SmartSliderSimple('#n2-ss-2', {
                "admin": false,
                "translate3d": 1,
                "callbacks": "",
                "randomize": {
                    "randomize": 0,
                    "randomizeFirst": 0
                },
                "align": "normal",
                "isDelayed": 0,
                "load": {
                    "fade": 1,
                    "scroll": 0
                },
                "playWhenVisible": 1,
                "playWhenVisibleAt": 0.5,
                "responsive": {
                    "desktop": 1,
                    "tablet": 1,
                    "mobile": 1,
                    "onResizeEnabled": true,
                    "type": "fullpage",
                    "downscale": 1,
                    "upscale": 1,
                    "minimumHeight": -1,
                    "maximumHeight": -1,
                    "maximumSlideWidth": 3000,
                    "maximumSlideWidthLandscape": 3000,
                    "maximumSlideWidthTablet": 3000,
                    "maximumSlideWidthTabletLandscape": 3000,
                    "maximumSlideWidthMobile": 3000,
                    "maximumSlideWidthMobileLandscape": 3000,
                    "maximumSlideWidthConstrainHeight": 0,
                    "forceFull": 1,
                    "forceFullOverflowX": "body",
                    "forceFullHorizontalSelector": "body",
                    "constrainRatio": 1,
                    "verticalOffsetSelectors": "#wpadminbar",
                    "focusUser": 0,
                    "focusAutoplay": 0,
                    "deviceModes": {
                        "desktopPortrait": 1,
                        "desktopLandscape": 0,
                        "tabletPortrait": 1,
                        "tabletLandscape": 0,
                        "mobilePortrait": 1,
                        "mobileLandscape": 0
                    },
                    "normalizedDeviceModes": {
                        "unknownUnknown": ["unknown", "Unknown"],
                        "desktopPortrait": ["desktop", "Portrait"],
                        "desktopLandscape": ["desktop", "Portrait"],
                        "tabletPortrait": ["tablet", "Portrait"],
                        "tabletLandscape": ["tablet", "Portrait"],
                        "mobilePortrait": ["mobile", "Portrait"],
                        "mobileLandscape": ["mobile", "Portrait"]
                    },
                    "verticalRatioModifiers": {
                        "unknownUnknown": 1,
                        "desktopPortrait": 1,
                        "desktopLandscape": 1,
                        "tabletPortrait": 1,
                        "tabletLandscape": 1,
                        "mobilePortrait": 1,
                        "mobileLandscape": 1
                    },
                    "minimumFontSizes": {
                        "desktopPortrait": 1,
                        "desktopLandscape": 1,
                        "tabletPortrait": 1,
                        "tabletLandscape": 1,
                        "mobilePortrait": 1,
                        "mobileLandscape": 1
                    },
                    "ratioToDevice": {
                        "Portrait": {
                            "tablet": 0.7,
                            "mobile": 0.5
                        },
                        "Landscape": {
                            "tablet": 0,
                            "mobile": 0
                        }
                    },
                    "sliderWidthToDevice": {
                        "desktopPortrait": 1920,
                        "desktopLandscape": 1920,
                        "tabletPortrait": 1344,
                        "tabletLandscape": 0,
                        "mobilePortrait": 960,
                        "mobileLandscape": 0
                    },
                    "basedOn": "combined",
                    "tabletPortraitScreenWidth": 800,
                    "mobilePortraitScreenWidth": 440,
                    "tabletLandscapeScreenWidth": 800,
                    "mobileLandscapeScreenWidth": 440,
                    "orientationMode": "width_and_height",
                    "scrollFix": 0,
                    "overflowHiddenPage": 0,
                    "desktopPortraitScreenWidth": 1200
                },
                "controls": {
                    "scroll": 0,
                    "drag": 1,
                    "touch": "horizontal",
                    "keyboard": 1,
                    "tilt": 0
                },
                "lazyLoad": 0,
                "lazyLoadNeighbor": 0,
                "blockrightclick": 0,
                "maintainSession": 0,
                "autoplay": {
                    "enabled": 1,
                    "start": 1,
                    "duration": 8000,
                    "autoplayToSlide": -1,
                    "autoplayToSlideIndex": -1,
                    "allowReStart": 0,
                    "pause": {
                        "click": 0,
                        "mouse": "enter",
                        "mediaStarted": 1
                    },
                    "resume": {
                        "click": 0,
                        "mouse": "leave",
                        "mediaEnded": 1,
                        "slidechanged": 0
                    }
                },
                "perspective": 1000,
                "layerMode": {
                    "playOnce": 0,
                    "playFirstLayer": 1,
                    "mode": "skippable",
                    "inAnimation": "mainInEnd"
                },
                "parallax": {
                    "enabled": 1,
                    "mobile": 0,
                    "is3D": 0,
                    "animate": 1,
                    "horizontal": "mouse",
                    "vertical": "mouse",
                    "origin": "slider",
                    "scrollmove": "both"
                },
                "background.parallax.tablet": 0,
                "background.parallax.mobile": 0,
                "postBackgroundAnimations": 0,
                "initCallbacks": [],
                "allowBGImageAttachmentFixed": false,
                "bgAnimations": 0,
                "mainanimation": {
                    "type": "horizontal",
                    "duration": 800,
                    "delay": 0,
                    "ease": "easeOutQuad",
                    "parallax": 1,
                    "shiftedBackgroundAnimation": "auto"
                },
                "carousel": 1,
                "dynamicHeight": 0
            });
            new N2Classes.SmartSliderWidgetArrowImage("n2-ss-2", 1, 0.7, 0.5);
            new N2Classes.SmartSliderWidgetBulletTransition("n2-ss-2", {
                "overlay": false,
                "area": 10,
                "thumbnails": [],
                "action": "click",
                "numeric": 0
            });
        });
    }));
</script>
