<!-- HEADER START -->
<div class="header-line-wrapper">
    <header class="type-1 plr120 fixed-top">
        <div class="table height-100p">
            <div class="table-row">
                <div class="logo-wrapper table-cell valign-middle">
                    <a href="{{env('APP_URL')}}" class="logo inline-block">
                        <img src="{{env('APP_URL')}}/assets/v1/images/logo-melko.png" alt="" />
                    </a>
                </div>
                <div class="main-menu relative table-cell valign-middle text-center text-sm-right">
                    <span class="toggle_menu">
                        <span></span>
                    </span>
                    <ul class="menu-1 fsize-0 clearfix">
                        @foreach(System::getMenu() as $menu)
                            <li class="inline-block  relative  
                                @if (isset($menu_id))
                                    @if($menu['id']===$menu_id) active @endif
                                @endif">
                                <a href="{{$menu['url']}}">
                                    {{$menu['name']}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="top-bar-social table-cell valign-middle text-right">
                    <ul class="social-list social-white fsize-0">
                        @foreach(System::getSocialLinks() as $item)
                            <li class="inline-block hidden-xs">
                                <a href="{{$item['url']}}" target="_blank">
                                    <div class="{{$item['class']}} animateScale border-1-w bdrs-50p fsize-14 color-white text-center square40">
                                        <i class="fa fa-{{$item['name']}}" aria-hidden="true"></i>
                                    </div>
                                </a>
                            </li>
                        @endforeach()
                    </ul>
                </div>
            </div>
        </div>
    </header>
</div>
<!-- HEADER END -->
