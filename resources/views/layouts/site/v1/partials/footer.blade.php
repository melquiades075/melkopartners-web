<footer class="type-1 background-2">
    <div class="container pt100">
        <div class="row text-center color-7 mb120">
            <div class="col-lg-3 col-md-3 col-sm-6 col-sx-12">
                <a href="index.html" class="block mb30">
                    <img src="{{env('APP_URL')}}/assets/v1/images/logo-melko.png" alt="" />
                </a>
                <span class="inline-block lheight-30 mb10">
                    {{System::getSettings()['footer1']}}
                </span>
                <ul class="social-list social-white fsize-0">
                    @foreach(System::getSocialLinks() as $item)
                        <li class="inline-block">
                            <a href="{{$item['url']}}" target="_blank">
                                <div class="{{$item['class']}} animateScale border-1-w bdrs-50p fsize-14 color-white text-center square40">
                                    <i class="fa fa-{{$item['name']}}" aria-hidden="true"></i>
                                </div>
                            </a>
                        </li>
                    @endforeach()
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-sx-12">
                <div class="fsize-24 fweight-500 color-white mb25 mt20">
                    {{System::getSettings()['footer2_title']}}
                </div>
                <div class="lheight-30">
                    {{System::getSettings()['address']}}
                    @foreach(System::getSettings()['phones'] as $phone)
                        <br/> Tel. {{$phone}}
                    @endforeach()
                    
                    @if(System::getSettings()['emails'])
                        @foreach(System::getSettings()['emails'] as $email)
                            <br/><a class="line_link color-white td-underline" href="mailto:{{$email}}" class="fsize-20 fweight-500 color-2 td-none">{{$email}}</a>
                        @endforeach()
                    @endif()
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-sx-12">
                <div class="fsize-24 fweight-500 color-white mb25 mt20">
                    {{System::getSettings()['footer3_title']}}
                </div>
                <div class="lheight-30">
                    @foreach(System::getMenu() as $menu)
                        <a href="{{$menu['url']}}" class="block color-7 no_line_link td-none">
                            {{$menu['name']}}
                        </a>
                    @endforeach                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 copyright text-center color-7 ptb30">
                © Copyright {{System::getSettings()['year']}} / {{System::getSettings()['title']}}
            </div>
        </div>
    </div>
</footer>
