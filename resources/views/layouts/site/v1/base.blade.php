<!DOCTYPE html>
<html lang="en">
    <head>
        @include('layouts.site.v1.partials.head')
    </head>
    <body class="{{$home or ''}}">
        @include('layouts.site.v1.partials.loader')
        @include('layouts.site.v1.partials.header')    
        @yield('content')
        @include('layouts.site.v1.partials.footer')
        @include('layouts.site.v1.partials.js-footer')
    </body>
</html>