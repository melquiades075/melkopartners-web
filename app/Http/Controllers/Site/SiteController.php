<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class SiteController extends Controller
{
    public function home()
    {
        //dd(System::getMenu());
       return view('layouts.site.v1.pages.home');
    }
}
