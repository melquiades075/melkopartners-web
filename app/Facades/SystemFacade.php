<?php
namespace App\Facades;
use Illuminate\Support\Collection;
use Illuminate\Support\Carbon;

class SystemFacade
{
    private $collection,$carbon;

    public function __construct()
    {
        $this->collection = new Collection();
        $this->carbon = new Carbon();
    }
    
    public function getCollection($data)
    {

        if(! is_object($data))
        {
            if(is_array($data))
            {
                return $this->collection->make($data);
            }

            return 'data no support';
        }
        elseif(is_object($data))
        {
            return $data;
        }

        return 'data no support';
    }

    public function getMenu()
    {
        $menu =[
            [
                'id'=> 1,
                'name' => 'Inicio', 
                'url'=>env('APP_URL'),
            ],
            [
                'id'=>2,
                'name' => 'Sobre M&P', 
                'url'=>env('APP_URL').'/sobre-melkopartners',
            ],
            /*
            [
                'id'=>3,
                'name' => 'Partners', 
                'url'=>env('APP_URL').'/partners',
            ],*/
            [
                'id'=>4,
                'name' => 'Contacto', 
                'url'=>env('APP_URL').'/contacto',
            ]
        ];

        return $this->getCollection($menu);
    }

    public function getSocialLinks()
    {
        $menu =[
            /*
            [
                'name' => 'facebook',
                'class'=>'facebook',
                'url'=>'https://www.drupal.org/',
            ],
            [
                'name' => 'twitter',
                'class'=>'twitter',
                'url'=>'https://www.drupal.org/',
            ],
            [
                'name' => 'instagram',
                'class'=>'google',
                'url'=>'https://www.drupal.org/',
            ],*/
        ];

        return  $this->getCollection($menu);
    }

    public function getSettings()
    {
        $settings =[
            'title' => 'Melko&Partners - MicroHolding',
            'favicon'=>'favicon.png',
            'logo'=>'logo-melko.png',
            'year'=> $this->carbon->now()->format('Y'),
            'address'=>'San Crescente 424',
            'phones'=>[
                '(02) 2 233 12 84',
            ],
            'emails'=>[
                'contacto@melkopartners.com',
            ],
            'footer1'=>'Melko&Partners es un “micro holding” compuesto por un conjunto de empresas internacionales',
            'footer2_title'=> 'Contacto',
            'footer3_title'=> 'Menu',
            'image_header' => 'section-title-bg.jpg',
            'client_label_button' => 'Visitar Web',
            '404_title' => 'Página no encontrada',
            '404_description' => 'Puedes intentar volviendo a nuestra pagina de inicio',
            '404_button' => 'Volver al Inicio',
            'contact_title' => 'Entra en contacto con nosotros',
            'contact_description' => 'Puedes comunicarte por medio de nuestro formulario de contacto, teléfonos o enviarnos un correo y te estaremos respondiendo en la mayor brevedad posible'
        ];

        return $this->getCollection($settings);
    }

    public function getSlides()
    {
        $slides =[
            'slides'=>
                [
                    'image'=>'slide_1.jpg',
                    'title'=>'<span class="color-15">Nuestro</span> Modelo',
                    'text'=>'Ofrece a los stakeholders la posibilidad de aportar a la generación de valor, a través de un<br> sistema sustentable que busca no solo la rentabilidad económica, sino también ofrecer <br>nuevas vías para innovar y contribuir al cambio de nuestra sociedad.',
                ],
                [
                    'image'=>'slide_2.jpg',
                    'title'=>'<span class="color-15">Decidamos</span> juntos',
                    'text'=>'Ofrecemos asesoría legal y contable centralizada como herramientas para conseguir tener mayores<br> insights de la información financiera que se maneja en relación a cada empresa.',
                ],
        ];

        return $this->getCollection($slides);
    }

    public function getPartners()
    {
        $partners =[
            'partners'=>
                [
                    'before_title'=>'Tecnología, agricultura y medicina',
                    'logo'=>'tecagromed-logo.png',
                    'images'=>'tecagromed-image.jpg',
                    'name'=> 'Tecagromed',
                    'title'=>'Titulo del slide 1',
                    'description'=>'Es una plataforma de distribución relacionada con tecnología, agricultura y medicina. A través de nuestros productos, creamos valor a los clientes y entregamos soluciones innovadoras enfocadas en la competitividad y la sustentabilidad.',
                    'url'=>'http://tecagromed.com/'
                ],
                [
                    'before_title'=>'Clientes Incognito',
                    'logo'=>'iq360-logo.png',
                    'images'=>'iq360-image.jpg',
                    'name'=> 'IQ360',
                    'title'=>'Titulo del slide 1',
                    'description'=>'Es una empresa especialista en medición de estándares de calidad que determina variables críticas a evaluar y de forma sutil levanta información in-situ, para su posterior análisis. Esta información sirve para la toma de decisiones, tácticas y estratégicas.                    ',
                    'url'=>'http://iq360.cl/'
                ],
                [
                    'before_title'=>'Software Development.',
                    'logo'=>'moviit-logo.png',
                    'images'=>'moviit-image.jpg',
                    'name'=> 'Moviit',
                    'title'=>'Titulo del slide 1',
                    'description'=>'Es una empresa dedicada al desarrollo de software, ofrece a sus clientes soluciones y servicios utilizando las ultimas tecnologicas del mercado.',
                    'url'=>'http://moviit.com/'
                ],
                [
                    'before_title'=>'Soluciones TICA líder en la región',
                    'logo'=>'23kycorp-logo.png',
                    'images'=>'23ky-image.jpg',
                    'name'=> '23kycorp',
                    'title'=>'Titulo del slide 1',
                    'description'=>'Empresa dedicada a otorgar valor y calidad que se traduzca en rentabilidad y éxito para nosotros y nuestros clientes. Esto a través de un servicio integral que incorpora sólidas herramientas humanas y técnicas, que nos permitan ser reconocida como una empresa con estilo propio movida constantemente hacia el equilibrio entre la rentabilidad, eficiencia, mejora continua, calidad humana y soluciones.',
                    'url'=>'http://23kycorp.com/'
                ],

        ];

        return $this->getCollection($partners);
    }
    
    public function getWeWork()
    {
        $we_work =[
            'we_work'=>
                [
                    'before_title'=>'¿Cómo Trabajamos?',
                    'title'=>'INCUBADORA',
                    'image'=>'testimonials_1.jpg',
                    'description'=> 'Establece criterios de selección para los proyectos candidatos a integrarse en el modelo de negocio desde la viabilidad técnica, económica y financiera, hasta el sector de actividad.',
                ],
                [
                    'before_title'=>'¿Cómo Trabajamos?',
                    'title'=>'ACELERADORA',
                    'image'=>'testimonials_3.jpg',
                    'description'=> '¡Logra aterrizar ideas además de ofrecer un espacio donde trabajar! De esa forma buscamos poder ofrecer el mismo modelo de negocio sustentable a Startups que coincidan con nuestra cultura “inter”',
                ],
                [
                    'before_title'=>'¿Cómo Trabajamos?',
                    'title'=>'INTERNACIONALIZACIÓN',
                    'image'=>'testimonials_2.jpg',
                    'description'=> 'Te ponemos en contacto con socios locales que puedan estar interesados en desarrollar tu idea.',
                ],                
                        
        ];
        
        return $this->getCollection($we_work);
    }
}
