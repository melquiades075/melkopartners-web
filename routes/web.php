<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Site')->group(function () {
        
    Route::view(
                '/', 
                'layouts.site.v1.pages.home',
                [   
                    'menu_id' => 1,
                    'home'=>'homepage'
                ]);

    Route::view(
                '/sobre-melkopartners', 
                'layouts.site.v1.pages.about-us',
                [   
                    'menu_id' => 2,
                    'title'=>'Nosotros'                    
                ]);
    /*
    Route::view(
                '/partners', 
                'layouts.site.v1.pages.partners',
                [   
                    'menu_id' => 3,
                    'title'=>'Nuestros Partners'
                ]);
    */

    Route::view(
                '/nuevo-modelo',
                'layouts.site.v1.pages.model',
                [   
                    'menu_id' => 0,
                    'title'=>'Nuestro Modelo'
                ])->name('model');
    Route::view(
                    '/contacto',
                    'layouts.site.v1.pages.contact',
                    [   
                        'menu_id' => 4,
                        'title'=>'Contáctanos',
                    ]);
                    
                    
});
