"use strict";

// JavaScript Document

( function( $ )
{
	var winObj = $( window ),
		bodyObj = $( 'body' ),
		headerObj = $( 'header' );

	winObj.on( 'load', function()
	{
        var $preloader = $( '.loader-wrapper' );
			
		$preloader.find( '.e-loadholder' ).fadeOut();
        $preloader.delay( 350 ).fadeOut( 'slow' );
    } );

	particlesJS( 'particleCanvas-Blue', {
		particles: {
			number: {
				value: 100,
				density: {
					enable: true,
					value_area: 800
				}
			},
			color: {
				value: "#1B5F70"
			},
			shape: {
				type: "circle",
				stroke: {
					width: 0,
					color: "#000000"
				},
				polygon: {
					nb_sides: 3
				},
				image: {
					src: "img/github.svg",
					width: 100,
					height: 100
				}
			},
			opacity: {
				value: 0.5,
				random: false,
				anim: {
					enable: true,
					speed: 1,
					opacity_min: 0.1,
					sync: false
				}
			},
			size: {
				value: 10,
				random: true,
				anim: {
					enable: false,
					speed: 10,
					size_min: 0.1,
					sync: false
				}
			},
			line_linked: {
				enable: false,
				distance: 150,
				color: "#ffffff",
				opacity: 0.4,
				width: 1
			},
			move: {
				enable: true,
				speed: 0.5,
				direction: "none",
				random: true,
				straight: false,
				out_mode: "bounce",
				bounce: false,
				attract: {
					enable: false,
					rotateX: 394.57382081613633,
					rotateY: 157.82952832645452
				}
			}
		},
		interactivity: {
			detect_on: "canvas",
			events: {
				onhover: {
					enable: true,
					mode: "grab"
				},
				onclick: {
					enable: false,
					mode: "push"
				},
				resize: true
			},
			modes: {
				grab: {
					distance: 200,
					line_linked: {
						opacity: 0.2
					}
				},
				bubble: {
					distance: 1500,
					size: 40,
					duration: 7.272727272727273,
					opacity: 0.3676323676323676,
					speed: 3
				},
				repulse: {
					distance: 50,
					duration: 0.4
				},
				push: {
					particles_nb: 4
				},
				remove: {
					particles_nb: 2
				}
			}
		},
		retina_detect: true
	} );
	
	particlesJS( 'particleCanvas-White', {
		particles: {
			number: {
				value: 250,
				density: {
					enable: true,
					value_area: 800
				}
			},
			color: {
				value: "#ffffff"
			},
			shape: {
				type: "circle",
				stroke: {
					width: 0,
					color: "#000000"
				},
				polygon: {
					nb_sides: 3
				},
				image: {
					src: "img/github.svg",
					width: 100,
					height: 100
				}
			},
			opacity: {
				value: 0.5,
				random: true,
				anim: {
					enable: false,
					speed: 0.2,
					opacity_min: 0,
					sync: false
				}
			},
			size: {
				value: 15,
				random: true,
				anim: {
					enable: true,
					speed: 10,
					size_min: 0.1,
					sync: false
				}
			},
			line_linked: {
				enable: false,
				distance: 150,
				color: "#ffffff",
				opacity: 0.4,
				width: 1
			},
			move: {
				enable: true,
				speed: 0.5,
				direction: "none",
				random: true,
				straight: false,
				out_mode: "bounce",
				bounce: false,
				attract: {
					enable: true,
					rotateX: 3945.7382081613637,
					rotateY: 157.82952832645452
				}
			}
		},
		interactivity: {
			detect_on: "canvas",
			events: {
				onhover: {
					enable: false,
					mode: "grab"
				},
				onclick: {
					enable: false,
					mode: "push"
				},
				resize: true
			},
			modes: {
				grab: {
					distance: 200,
					line_linked: {
						opacity: 0.2
					}
				},
				bubble: {
					distance: 1500,
					size: 40,
					duration: 7.272727272727273,
					opacity: 0.3676323676323676,
					speed: 3
				},
				repulse: {
					distance: 50,
					duration: 0.4
				},
				push: {
					particles_nb: 4
				},
				remove: {
					particles_nb: 2
				}
			}
		},
		retina_detect: true
	} );

	/*----------------------------------------------------*/
	/* Adaptive Menu Width
	/*----------------------------------------------------*/

	var ulMenu1 = $( 'ul.menu-1' );
	winObj.on( 'resize', function()
	{		
		if( $( this ).width() > 992 )
		{
			ulMenu1.flexMenu();
		}
	} );

	if( winObj.width() > 992 )
	{
		ulMenu1.flexMenu();
	}

	headerObj.on( 'click', '.flexMenu-viewMore', function()
	{
		$( this ).toggleClass( 'active' );
		$( '.flexMenu-popup' ).attr( 'style', 'display: block;' )
	} );
	
	/*----------------------------------------------------*/
	/* Fixed Menu
	/*----------------------------------------------------*/
	var nav1 = $( '.header-line-wrapper' ),
		nav = $( 'header:not(.type-1)' ),
		navHide = $( '.affixed-hidden' );
		
	winObj.on( 'scroll', function()
	{
		if( $( this ).scrollTop() > 0 )
		{
			nav1.addClass( 'affix-top' );
		}
		else
		{
			nav1.removeClass( 'affix-top' );
		}
		
		if( $( this ).scrollTop() > navHide.height() )
		{
			nav.addClass( 'affix-top' );
		}
		else
		{
			nav.removeClass( 'affix-top' );
		}
	} );

	/*----------------------------------------------------*/
	/* Toggle menu
	/*----------------------------------------------------*/
	headerObj.on( 'click', '.toggle_menu', function()
	{
		$( this ).toggleClass( 'open' );
		if( $( this ).hasClass( 'open' ) )
		{
			$( '.menu-1' ).addClass( 'open' );
			bodyObj.addClass('no-scroll');
		}
		else
		{
			$( '.menu-1' ).removeClass( 'open' );
			$( '.menu-item-has-children' ).removeClass( 'open-list' );
			bodyObj.removeClass( 'no-scroll' );
		}
	} );

	/*----------------------------------------------------*/
	/* Parallax Initialize
	/*----------------------------------------------------*/
	$( '.parallax' ).paroller();

	/*----------------------------------------------------*/
	/* WOW Initialize
	/*----------------------------------------------------*/
	new WOW().init();

	/*----------------------------------------------------*/
	/* Equal height
	/*----------------------------------------------------*/
	$( '.equal-height' ).matchHeight();

	/*----------------------------------------------------*/
	/* Mobile Sub Menu
	/*----------------------------------------------------*/
	headerObj.on( 'click', '.menu-1.open a', function( e )
	{
		if( $( this ).siblings().length )
		{
			if( !$( this ).parent().hasClass( 'open-list' ) )
			{
				$( this ).parent().addClass( 'open-list' );
				e.preventDefault();
			}
			else
			{
				$( this ).parent().removeClass( 'open-list' );
			}
		}
	} );

	/*----------------------------------------------------*/
	/* Show/Hide Modal Window
	/*----------------------------------------------------*/
	$( '[data-target]' ).on( 'click', function( e )
	{
		e.preventDefault();

		var target = $( this ).data( 'target' );
		$( target ).addClass( 'show' );
	} );

	var modals = $( '.modal' );
	bodyObj.keydown( function( e )
	{
		if ( e.keyCode === 27 )
		{
			modals.removeClass( 'show' );
		}
	} );

	modals.on( 'click', function( e )
	{
		if( $( e.target ).hasClass( 'modal' ) )
		{
			modals.removeClass( 'show' );
		}
	} );

	// Disabled/Enabled Button in Registration Modal
	$( '.rules' ).on( 'click', '.chackbox_rules', function()
	{
		var btnReg = $( '.registration-modal .btn-reg' );
		$( this ).parent().toggleClass( 'checked' );
		if( $( this ).parent().hasClass( 'checked' ) )
		{
			btnReg.removeAttr( 'disabled' );
		}
		else
		{
			btnReg.attr( 'disabled', 'disabled' );
		}
	} );

	/*----------------------------------------------------*/
	/*	Trimming
	/*----------------------------------------------------*/

	// Trimming text
	var dataTrimText = $( '[data-trim-text]' );
	dataTrimText.each( function()
	{
		var stringLength = $( this ).attr( 'data-trim-text' ),
			string = $( this ).text().trim();
		if( string.length > stringLength )
		{
			$( this ).text( string.slice( 0, stringLength - 3 ) ).append( '<span class="more_btn inline-block text-gradient uppercase pointer fsize-14 fweight-600 ml5">Read More <i class="fa fa-level-down text-gradient" aria-hidden="true"></i></span>' );
		}

		bodyObj.on( 'click', '.more_btn', function()
		{
			$( this ).attr( 'style', 'display: none' );
			$( this ).parent().text( string.slice() );
		} );
	} );

	// Trimming string
	var dataTrim = $( '[data-trim]' );
	dataTrim.each( function()
	{
		var stringLength = $( this ).attr( 'data-trim' ),
			string = $( this ).text().trim();
		if( string.length > stringLength )
		{
			$( this ).text( string.slice( 0, stringLength - 3 ) + '...' );
		}
	} );

	/*----------------------------------------------------*/
	/*	Progress Bar
	/*----------------------------------------------------*/

	function progressBarCreator( count, animate )
	{
		var bar = new ProgressBar.Line( count, {
			strokeWidth: 1,
			color: '#fff',
			trailColor: '#fff',
			trailWidth: 1,
			easing: 'easeInOut',
			duration: 10000,
			svgStyle: null,
			text: {
				value: '',
				alignToBottom: false
			},
			from: {
				color: '#00cbd6'
			},
			to: {
				color: '#0098d6'
			},
			// Set default step function for all animate calls
			step: function( state, bar )
			{
				bar.path.setAttribute( 'stroke', state.color );
				var value = ( Math.round( bar.value() * 100 ) + '%' );
				if( value === 0 )
				{
					bar.setText( '' );
				}
				else
				{
					bar.setText( value );
				}

				bar.text.style.color = state.color;
			}
		} );

		bar.text.style.fontFamily = '"Montserrat"';
		bar.text.style.fontSize = '12px';

		bar.animate( animate, {
			duration: 2500
		}, function() {} );
	}

	var mainLine = $( '.skills' );
	if( mainLine.length )
	{
		mainLine.on( 'inview', function ( event, isInView )
		{
			if( isInView )
			{
				if( !$( this ).hasClass( 'visible' ) )
				{
					$( this ).addClass( 'visible' );
					
					progressBarCreator( progressline86, 0.86 );
					progressBarCreator( progressline80, 0.80 );
					progressBarCreator( progressline52, 0.52 );
				}
			}
		} );
	}

	var mainSkills = $( '.services-items' );
	if( mainSkills.length )
	{
		progressBarCreator( progressline69, 0.69 );
		progressBarCreator( progressline80, 0.80 );
		progressBarCreator( progressline52, 0.52 );
		progressBarCreator( progressline49, 0.49 );
		progressBarCreator( progressline20, 0.20 );
		progressBarCreator( progressline72, 0.72 );
		progressBarCreator( progressline37, 0.37 );
		progressBarCreator( progressline91, 0.91 );
		progressBarCreator( progressline25, 0.25 );
		progressBarCreator( progressline58, 0.58 );
		progressBarCreator( progressline23, 0.23 );
		progressBarCreator( progressline83, 0.83 );
		progressBarCreator( progressline10, 0.10 );
		progressBarCreator( progressline65, 0.65 );
		progressBarCreator( progressline82, 0.82 );
	}

	var serviceSkills = $( '.service-skills' );
	if( serviceSkills.length )
	{
		serviceSkills.on( 'inview', function( event, isInView )
		{
			if( isInView )
			{
				if( !$( this ).hasClass( 'visible' ) )
				{
					$( this ).addClass( 'visible' );

					progressBarCreator( progressline37, 0.37 );
					progressBarCreator( progressline91, 0.91 );
					progressBarCreator( progressline25, 0.25 );
					progressBarCreator( progressline65, 0.65 );
				}
			}
		} );
	}

	/*----------------------------------------------------*/
	/*	Video Settings
	/*----------------------------------------------------*/
	$( '#buttonbar' ).on( 'click', '.play', function()
	{
		var video = $( '.video-section video' ),
			button = $( '#play' );
		if( video[ 0 ].paused )
		{
			video[ 0 ].play();
			$( this ).addClass( 'pause-show' );
			$( this ).removeClass( 'play-show' );
		}
		else
		{
			video[ 0 ].pause();
			$( this ).addClass( 'play-show' );
			$( this ).removeClass( 'pause-show' );
		}
	} );

	/*----------------------------------------------------*/
	/* Masonry Settings
	/*----------------------------------------------------*/
	var alignCont = $( '.alignment-container' );
	if( alignCont.length )
	{
		alignCont.masonry( {
			itemSelector: '.alignment-item',
			columnWidth: '.alignment-item',
			singleMode: true,
			isResizable: true,
			percentPosition: true
		} );
	}

	/*----------------------------------------------------*/
	/* LightBox Initialize
	/*----------------------------------------------------*/

	lightbox.option( {
		showImageNumberLabel: false
	} );

	/*----------------------------------------------------*/
	/* Muuri Settings
	/*----------------------------------------------------*/

	$( '.portfolio-page' ).on( 'click', '.filter_container > div', function()
	{
		$( '.filter_container > div' ).removeClass( 'active' );
		$( this ).addClass( 'active' );
	} );

	if( $( '.item_container' ).length )
	{
		var itemGrid = new Muuri( '.item_container', {
			showDuration: 200,
			hideDuration: 100,
			showEasing: 'ease-out',
			layout: {
				rounding: false
			}
		} );

		$( '.filter_container .filter-item' ).on( 'click', function()
		{
			var filterClass = $( this ).data( 'filter' );
			if( filterClass === 'all' )
			{
				itemGrid.filter( '.item' );
			}
			else
			{
				itemGrid.filter( '.' + filterClass );
			}
		} );
	}

	/*----------------------------------------------------*/
	/* Calendar Settings
	/*----------------------------------------------------*/

	var calendarBl = $( '.calendar-bl' );
	if( calendarBl.length )
	{
		calendarBl.dateRangePicker( {
			inline: true,
			container: '.calendar-container',
			alwaysOpen: true,
			singleDate: true,
			singleMonth: true,
			showTopbar: false,
			customArrowPrevSymbol: '<i class="fa fa-angle-left"></i>',
			customArrowNextSymbol: '<i class="fa fa-angle-right"></i>'
		} );
	}

	/*----------------------------------------------------*/
	/* Sliders Settings
	/*----------------------------------------------------*/

	$( '.format-gallery .item-media-slider' ).slick( {
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		dots: true
	} );

	$( '.testimonials-slider' ).slick( {
		arrows: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		draggable: true,
		dots: true,
		respondTo: 'window',
		autoplay: true
	} );

	$( '.about-testimonials .testimonial-nav' ).slick( {
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.testimonial-content',
		arrow: false,
		centerMode: true,
		variableWidth: true,
		focusOnSelect: true
	} );

	$( '.about-testimonials .testimonial-content' ).slick( {
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		dots: true,
		fade: true,
		adaptiveHeight: true,
		asNavFor: '.testimonial-nav',
		responsive: [ {
			breakpoint: 768,
			settings: {
				arrows: false
			}
		} ]
	} );

	$( '.post-testimonials-slider .testimonial-nav' ).slick( {
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.testimonial-content',
		arrow: false,
		centerMode: true,
		variableWidth: true,
		focusOnSelect: true
	} );

	$( '.post-testimonials-slider .testimonial-content' ).slick( {
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		dots: true,
		fade: true,
		asNavFor: '.testimonial-nav',
		responsive: [ {
			breakpoint: 768,
			settings: {
				arrows: false
			}
		} ]
	} );

	$( '.client-slider' ).slick( {
		arrows: false,
		slidesToShow: 8,
		slidesToScroll: 1,
		infinite: true,
		draggable: true,
		dots: false,
		autoplay: true,
		responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	} );

	$( '.service-gallery' ).slick( {
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		infinite: true,
		autoplay: true,
		variableWidth: true,
		centerMode: true
	} );

	$( '.blog-gallery' ).slick( {
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		dots: true,
		infinite: true,
		autoplay: true
	} );

	$( '.nav-tabs a' ).on( 'click', function()
	{
		$( this ).tab( 'show' );
	} );

	function teamSliderSlick( selector )
	{
		if( !selector.hasClass( 'slick-initialized' ) )
		{
			selector.slick( {
				arrows: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: false,
				draggable: true,
				respondTo: 'slider'
			} );
		}
	}

	var tabContent = $( '.tab-content' );

	teamSliderSlick( tabContent.find( '.active .team-slider' ) );

	$( '.nav-tabs a' ).on( 'shown.bs.tab', function( event )
	{
		teamSliderSlick( tabContent.find( '.active .team-slider') );
	} );

	/*----------------------------------------------------*/
	/*	Animated Scroll To Top
	/*----------------------------------------------------*/
	var toTop = $( '#toTop' );
	toTop.on( 'click', function()
	{
		$( 'html, body' ).animate( {
			scrollTop: 0
		}, 600 );
		return false;
	} );

	winObj.on( 'scroll', function()
	{
		if( $( this ).scrollTop() != 0 )
		{
			toTop.fadeIn();
		}
		else
		{
			toTop.fadeOut();
		}
	} );

	/*----------------------------------------------------*/
	/*	Google maps
	/*----------------------------------------------------*/
	if( $( '#map' ).length )
	{
		function initialize()
		{
			var mapOptions = {
				center: {
					lat: 50.518759,
					lng: 30.239388
				},
				zoom: 15,
				styles: [ {
						"featureType": "all",
						"elementType": "labels.text.fill",
						"stylers": [ { "saturation": 36 }, { "color": "#000000" }, { "lightness": 40 } ]
					},
					{
						"featureType": "all",
						"elementType": "labels.text.stroke",
						"stylers": [ { "visibility": "on" }, { "color": "#000000" }, { "lightness": 16 } ]
					},
					{
						"featureType": "all",
						"elementType": "labels.icon",
						"stylers": [ { "visibility": "off" } ]
					},
					{
						"featureType": "administrative",
						"elementType": "geometry.fill",
						"stylers": [ { "color": "#000000" }, { "lightness": 20 } ]
					},
					{
						"featureType": "administrative",
						"elementType": "geometry.stroke",
						"stylers": [ { "color": "#000000" }, { "lightness": 17 }, { "weight": 1.2 } ]
					},
					{
						"featureType": "landscape",
						"elementType": "geometry",
						"stylers": [ { "color": "#000000" }, { "lightness": 20 } ]
					},
					{
						"featureType": "poi",
						"elementType": "geometry",
						"stylers": [ { "color": "#000000" }, { "lightness": 21 } ]
					},
					{
						"featureType": "road.highway",
						"elementType": "geometry.fill",
						"stylers": [ { "color": "#000000" }, { "lightness": 17 } ]
					},
					{
						"featureType": "road.highway",
						"elementType": "geometry.stroke",
						"stylers": [ { "color": "#000000" }, { "lightness": 29 }, { "weight": 0.2 } ]
					},
					{
						"featureType": "road.arterial",
						"elementType": "geometry",
						"stylers": [ { "color": "#000000" }, { "lightness": 18 } ]
					},
					{
						"featureType": "road.local",
						"elementType": "geometry",
						"stylers": [ { "color": "#000000" }, { "lightness": 16 } ]
					},
					{
						"featureType": "transit",
						"elementType": "geometry",
						"stylers": [ { "color": "#000000" }, { "lightness": 19 } ]
					},
					{
						"featureType": "water",
						"elementType": "geometry",
						"stylers": [ { "color": "#000000" }, { "lightness": 17 } ]
					}
				],
				disableDefaultUI: true,
				scrollwheel: false,
				draggable: true
			};

			var map = new google.maps.Map( document.getElementById( 'map' ), mapOptions );

			// Create marker
			var marker = new google.maps.Marker( {
				position: {
					lat: 50.518759,
					lng: 30.239388
				},
				map: map,
				title: 'RILIANT',
				animation: google.maps.Animation.DROP,
				icon: '../assets/images/marker.png'
			} );

			// Marker animation
			marker.setAnimation( google.maps.Animation.BOUNCE );
		}

		google.maps.event.addDomListener( window, 'load', initialize );
	}

}( jQuery ) );